import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyD1KYtLRthtcDSDNPxu4-CLKT4qb4kgCgk',
  authDomain: 'my-doctor-by-raka.firebaseapp.com',
  databaseURL: 'https://my-doctor-by-raka.firebaseio.com',
  projectId: 'my-doctor-by-raka',
  storageBucket: 'my-doctor-by-raka.appspot.com',
  messagingSenderId: '517609549516',
  appId: '1:517609549516:web:c7ec82b9de6e1d456d9c69',
});

const Fire = firebase;

export default Fire;
